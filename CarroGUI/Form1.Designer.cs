﻿namespace CarroGUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MatriculaLabel = new System.Windows.Forms.Label();
            this.DepositoLabel = new System.Windows.Forms.Label();
            this.ContadorViagemLabel = new System.Windows.Forms.Label();
            this.KilototalLabel = new System.Windows.Forms.Label();
            this.LitroTrackBar = new System.Windows.Forms.TrackBar();
            this.EncherButton = new System.Windows.Forms.Button();
            this.ViajarButton = new System.Windows.Forms.Button();
            this.EurosLabel = new System.Windows.Forms.Label();
            this.CheioLabel = new System.Windows.Forms.Label();
            this.LitrosTrackLabel = new System.Windows.Forms.Label();
            this.TotalEurosLabel = new System.Windows.Forms.Label();
            this.PrecoMedioKmLabel = new System.Windows.Forms.Label();
            this.KmBox = new System.Windows.Forms.TextBox();
            this.IndicacaoKmLabel = new System.Windows.Forms.Label();
            this.KmPossiveisLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.LitroTrackBar)).BeginInit();
            this.SuspendLayout();
            // 
            // MatriculaLabel
            // 
            this.MatriculaLabel.AutoSize = true;
            this.MatriculaLabel.Location = new System.Drawing.Point(49, 9);
            this.MatriculaLabel.Name = "MatriculaLabel";
            this.MatriculaLabel.Size = new System.Drawing.Size(35, 13);
            this.MatriculaLabel.TabIndex = 0;
            this.MatriculaLabel.Text = "label1";
            // 
            // DepositoLabel
            // 
            this.DepositoLabel.AutoSize = true;
            this.DepositoLabel.Location = new System.Drawing.Point(49, 58);
            this.DepositoLabel.Name = "DepositoLabel";
            this.DepositoLabel.Size = new System.Drawing.Size(35, 13);
            this.DepositoLabel.TabIndex = 1;
            this.DepositoLabel.Text = "label2";
            // 
            // ContadorViagemLabel
            // 
            this.ContadorViagemLabel.AutoSize = true;
            this.ContadorViagemLabel.Location = new System.Drawing.Point(174, 9);
            this.ContadorViagemLabel.Name = "ContadorViagemLabel";
            this.ContadorViagemLabel.Size = new System.Drawing.Size(35, 13);
            this.ContadorViagemLabel.TabIndex = 2;
            this.ContadorViagemLabel.Text = "label3";
            // 
            // KilototalLabel
            // 
            this.KilototalLabel.AutoSize = true;
            this.KilototalLabel.Location = new System.Drawing.Point(174, 58);
            this.KilototalLabel.Name = "KilototalLabel";
            this.KilototalLabel.Size = new System.Drawing.Size(35, 13);
            this.KilototalLabel.TabIndex = 3;
            this.KilototalLabel.Text = "label4";
            // 
            // LitroTrackBar
            // 
            this.LitroTrackBar.Location = new System.Drawing.Point(23, 213);
            this.LitroTrackBar.Maximum = 100;
            this.LitroTrackBar.Name = "LitroTrackBar";
            this.LitroTrackBar.Size = new System.Drawing.Size(244, 45);
            this.LitroTrackBar.TabIndex = 4;
            this.LitroTrackBar.Scroll += new System.EventHandler(this.LitroTrackBar_Scroll);
            // 
            // EncherButton
            // 
            this.EncherButton.Location = new System.Drawing.Point(94, 277);
            this.EncherButton.Name = "EncherButton";
            this.EncherButton.Size = new System.Drawing.Size(75, 23);
            this.EncherButton.TabIndex = 5;
            this.EncherButton.Text = "Encher";
            this.EncherButton.UseVisualStyleBackColor = true;
            this.EncherButton.Click += new System.EventHandler(this.EncherButton_Click);
            // 
            // ViajarButton
            // 
            this.ViajarButton.Location = new System.Drawing.Point(94, 352);
            this.ViajarButton.Name = "ViajarButton";
            this.ViajarButton.Size = new System.Drawing.Size(75, 23);
            this.ViajarButton.TabIndex = 6;
            this.ViajarButton.Text = "Viajar";
            this.ViajarButton.UseVisualStyleBackColor = true;
            this.ViajarButton.Click += new System.EventHandler(this.ViajarButton_Click);
            // 
            // EurosLabel
            // 
            this.EurosLabel.AutoSize = true;
            this.EurosLabel.Location = new System.Drawing.Point(126, 245);
            this.EurosLabel.Name = "EurosLabel";
            this.EurosLabel.Size = new System.Drawing.Size(34, 13);
            this.EurosLabel.TabIndex = 7;
            this.EurosLabel.Text = "Euros";
            // 
            // CheioLabel
            // 
            this.CheioLabel.AutoSize = true;
            this.CheioLabel.Location = new System.Drawing.Point(50, 173);
            this.CheioLabel.Name = "CheioLabel";
            this.CheioLabel.Size = new System.Drawing.Size(0, 13);
            this.CheioLabel.TabIndex = 8;
            // 
            // LitrosTrackLabel
            // 
            this.LitrosTrackLabel.AutoSize = true;
            this.LitrosTrackLabel.Location = new System.Drawing.Point(125, 197);
            this.LitrosTrackLabel.Name = "LitrosTrackLabel";
            this.LitrosTrackLabel.Size = new System.Drawing.Size(32, 13);
            this.LitrosTrackLabel.TabIndex = 9;
            this.LitrosTrackLabel.Text = "Litros";
            // 
            // TotalEurosLabel
            // 
            this.TotalEurosLabel.AutoSize = true;
            this.TotalEurosLabel.Location = new System.Drawing.Point(100, 100);
            this.TotalEurosLabel.Name = "TotalEurosLabel";
            this.TotalEurosLabel.Size = new System.Drawing.Size(0, 13);
            this.TotalEurosLabel.TabIndex = 10;
            // 
            // PrecoMedioKmLabel
            // 
            this.PrecoMedioKmLabel.AutoSize = true;
            this.PrecoMedioKmLabel.Location = new System.Drawing.Point(91, 135);
            this.PrecoMedioKmLabel.Name = "PrecoMedioKmLabel";
            this.PrecoMedioKmLabel.Size = new System.Drawing.Size(85, 13);
            this.PrecoMedioKmLabel.TabIndex = 12;
            this.PrecoMedioKmLabel.Text = "Preco medio KM";
            // 
            // KmBox
            // 
            this.KmBox.Location = new System.Drawing.Point(79, 326);
            this.KmBox.Name = "KmBox";
            this.KmBox.Size = new System.Drawing.Size(100, 20);
            this.KmBox.TabIndex = 13;
            this.KmBox.TextChanged += new System.EventHandler(this.KmBox_TextChanged);
            // 
            // IndicacaoKmLabel
            // 
            this.IndicacaoKmLabel.AutoSize = true;
            this.IndicacaoKmLabel.Location = new System.Drawing.Point(67, 310);
            this.IndicacaoKmLabel.Name = "IndicacaoKmLabel";
            this.IndicacaoKmLabel.Size = new System.Drawing.Size(132, 13);
            this.IndicacaoKmLabel.TabIndex = 14;
            this.IndicacaoKmLabel.Text = "Quantos km deseja viajar?";
            // 
            // KmPossiveisLabel
            // 
            this.KmPossiveisLabel.AutoSize = true;
            this.KmPossiveisLabel.Location = new System.Drawing.Point(20, 71);
            this.KmPossiveisLabel.Name = "KmPossiveisLabel";
            this.KmPossiveisLabel.Size = new System.Drawing.Size(68, 13);
            this.KmPossiveisLabel.TabIndex = 15;
            this.KmPossiveisLabel.Text = "Km possiveis";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(279, 397);
            this.Controls.Add(this.KmPossiveisLabel);
            this.Controls.Add(this.IndicacaoKmLabel);
            this.Controls.Add(this.KmBox);
            this.Controls.Add(this.PrecoMedioKmLabel);
            this.Controls.Add(this.TotalEurosLabel);
            this.Controls.Add(this.LitrosTrackLabel);
            this.Controls.Add(this.CheioLabel);
            this.Controls.Add(this.EurosLabel);
            this.Controls.Add(this.ViajarButton);
            this.Controls.Add(this.EncherButton);
            this.Controls.Add(this.LitroTrackBar);
            this.Controls.Add(this.KilototalLabel);
            this.Controls.Add(this.ContadorViagemLabel);
            this.Controls.Add(this.DepositoLabel);
            this.Controls.Add(this.MatriculaLabel);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.LitroTrackBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label MatriculaLabel;
        private System.Windows.Forms.Label DepositoLabel;
        private System.Windows.Forms.Label ContadorViagemLabel;
        private System.Windows.Forms.Label KilototalLabel;
        private System.Windows.Forms.TrackBar LitroTrackBar;
        private System.Windows.Forms.Button EncherButton;
        private System.Windows.Forms.Button ViajarButton;
        private System.Windows.Forms.Label EurosLabel;
        private System.Windows.Forms.Label CheioLabel;
        private System.Windows.Forms.Label LitrosTrackLabel;
        private System.Windows.Forms.Label TotalEurosLabel;
        private System.Windows.Forms.Label PrecoMedioKmLabel;
        private System.Windows.Forms.TextBox KmBox;
        private System.Windows.Forms.Label IndicacaoKmLabel;
        private System.Windows.Forms.Label KmPossiveisLabel;
    }
}

