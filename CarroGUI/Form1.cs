﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CarroGUI
{
    public partial class Form1 : Form

    {
        private Carro meuCarro;
        int kmboxtemp;
        public Form1()
        {
            InitializeComponent();

            meuCarro = new Carro();


            MatriculaLabel.Text = "Matrícula" + meuCarro.Matricula;
            DepositoLabel.Text = "Depósito:" + meuCarro.Litrodeposito.ToString()+" litros";
            ContadorViagemLabel.Text = "Viagens realizadas:" + meuCarro.Contador.ToString();
            KilototalLabel.Text = "Km total:" + meuCarro.Kilototal.ToString() + "km";
            PrecoMedioKmLabel.Text = "Custo por km:" + Math.Round(meuCarro.CustoMedioKm(),2).ToString() + "€";
            KmPossiveisLabel.Text = "São possíveis fazer " + meuCarro.KmPossiveis().ToString() + " km com o combustível atual";
        }

        private void EncherButton_Click(object sender, EventArgs e)
        {


            meuCarro.EncherDeposito(LitroTrackBar.Value);
            if (meuCarro.DepositoCheio())
            {
                CheioLabel.Text = "Depósito cheio!";

                EncherButton.Enabled = false;

                DepositoLabel.Text = "Depósito:" + meuCarro.Litrodeposito.ToString() + " litros";

                
            }

            if (meuCarro.CombustivelExcedido())
            {
                CheioLabel.Text = "Combustível excedido, \n por favor selecione outra quantidade";

                meuCarro.Litrodeposito -= LitroTrackBar.Value;

                EurosLabel.Text = "";

                DepositoLabel.Text = "Depósito:" + meuCarro.Litrodeposito.ToString() + " litros";
                
            }
            
            else
            {
                DepositoLabel.Text = "Depósito:" + meuCarro.Litrodeposito.ToString() + " litros";
                meuCarro.Eurototal += meuCarro.CustoLitro(LitroTrackBar.Value);
                TotalEurosLabel.Text = "Total euros:" + meuCarro.Eurototal.ToString() + "€";
            }
            KmPossiveisLabel.Text = "São possíveis fazer " + meuCarro.KmPossiveis().ToString() + " km com o combustível atual";
        }

        private void LitroTrackBar_Scroll(object sender, EventArgs e)
        {
            LitrosTrackLabel.Text = LitroTrackBar.Value.ToString() + " litros";
            EurosLabel.Text = meuCarro.CustoLitro(LitroTrackBar.Value).ToString() + "euros";
        }

        private void ViajarButton_Click(object sender, EventArgs e)
        {
            meuCarro.Viajar(kmboxtemp);
            DepositoLabel.Text = "Depósito:" + meuCarro.Litrodeposito.ToString() + " litros";
            KmPossiveisLabel.Text = "São possíveis fazer " + meuCarro.KmPossiveis().ToString() + " km com o combustível atual";

        }

        private void KmBox_TextChanged(object sender, EventArgs e)
        {
            
            if (!int.TryParse(KmBox.Text, out kmboxtemp))
            {
                KmBox.Text = "";
            }
        }
    }
}
