﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarroGUI
{
    class Carro
    {
        #region Atributos

        private string _matricula;

        private int _capacidade;

        private int _litrodeposito;

        private int _contador;

        private int _kilototal;

        private int _consumomedio;

        private double _eurototal;



        #endregion

        #region Propriedades
        public string Matricula
        {
            get
            {
                return _matricula;
            }

            set
            {
                _matricula = value;
            }
        }

        public int Capacidade
        {
            get
            {
                return _capacidade;
            }

            set
            {
                _capacidade = value;
            }
        }

        public int Litrodeposito
        {
            get
            {
                return _litrodeposito;
            }

            set
            {
                _litrodeposito = value;
            }
        }

        public int Contador
        {
            get
            {
                return _contador;
            }

            set
            {
                _contador = value;
            }
        }

        public int Kilototal
        {
            get
            {
                return _kilototal;
            }

            set
            {
                _kilototal = value;
            }
        }

        public int Consumomedio
        {
            get
            {
                return _consumomedio;
            }

            set
            {
                _consumomedio = value;
            }
        }

        public double Eurototal
        {
            get
            {
                return _eurototal;
            }

            set
            {
                _eurototal = value;
            }
        }

        #endregion

        #region Construtores

        public Carro(string matricula, int capacidade, int litrodeposito)

        {
            Matricula = matricula;
            Capacidade = capacidade;
            Litrodeposito = litrodeposito;
            Contador = 0;
            Kilototal = 0;
            Consumomedio = 7;
            Eurototal = 0;
        }

        public Carro()
        {
            Matricula = "PL-04-EU";
            Capacidade = 50;
            Litrodeposito = 0;
            Contador = 0;
            Kilototal = 0;
            Consumomedio = 7;
            Eurototal = 0;
        }

        #endregion


        #region Métodos

        public void EncherDeposito(int litros)

        {
            Litrodeposito += litros;
            
        }

        public bool DepositoCheio()
        {
            if(Litrodeposito == 50)
            {
                return true;
                
            }
            else
            {
                return false;
            }
        }

        public bool CombustivelExcedido()

        {
            if (Litrodeposito > 50)
            {
                return true;

            }
            else
            {
                return false;
            }
        }
    

        public bool Reserva()

        {
            if (Litrodeposito > 10)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public double CustoLitro(double litros)

        {
            return litros * 1.18;
        }

        public double CustoMedioKm()

        {
            return  1.18 / (100 / 7);
        }

        public int KmPossiveis()
        {
            return (Litrodeposito * 100) / Consumomedio;
        }

        public void Viajar(int km)

        {
            km = (km * Consumomedio) / 100;

            Litrodeposito -= km;
        }
        
        #endregion


    }
}
